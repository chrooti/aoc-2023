def solve(steps: tuple[int, int]):
    perimeter = 0
    area = 0
    x = 0
    for (dy, dx), offset in steps:
        y_off = dy * offset
        x_off = dx * offset

        perimeter += abs(x_off) + abs(y_off)
        area += x * y_off
        x += x_off

    return area + perimeter // 2 + 1


def main():
    directions = {
        "R": (0, 1),
        "0": (0, 1),
        "D": (1, 0),
        "1": (1, 0),
        "L": (0, -1),
        "2": (0, -1),
        "U": (-1, 0),
        "3": (-1, 0),
    }

    with open("inputs/day18.txt", "r") as file:
        lines = tuple(
            (
                directions[direction],
                int(offset),
                int(color[2:-2], 16),
                directions[color[-2]],
            )
            for line in file.read().splitlines()
            for (direction, offset, color) in (line.split(),)
        )

    part_1 = solve(tuple((direction, offset) for direction, offset, _, _ in lines))
    part_2 = solve(tuple((direction, offset) for _, _, offset, direction in lines))

    return part_1, part_2


print(main())
