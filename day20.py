import math


def main():
    with open("inputs/day20.txt", "r") as file:
        lines = file.read().splitlines()

    parts = {
        part[1:]: (part[0], connections.split(", "))
        for line in lines
        for (part, connections) in (line.split(" -> "),)
    }

    cycles = []

    for starting_part in parts["roadcaster"][1]:
        part = starting_part

        # 1 in binary here means "sends output after 2^n pulses"
        binary = 0
        bin_exp = 0

        while part is not None:
            # assume flipflop
            _, connections = parts[part]

            is_connected_to_conjunction = any(
                c in parts and parts[c][0] == "&" for c in connections
            )
            binary += int(is_connected_to_conjunction) << bin_exp
            bin_exp += 1

            # next flipflop if any
            part = next(
                (c for c in connections if c in parts and parts[c][0] == "%"), None
            )

        cycles.append(binary)

    part_2 = math.lcm(*cycles)

    # part1 lost in ctrl+Z cache of a vscode crash <3

    return 866435264, part_2


print(main())
