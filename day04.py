def main():
    with open("inputs/day04.txt", "r") as file:
        lines = file.read().splitlines()

    card_points = tuple(
        len(
            {int(num) for num in winning_num.split()}
            & {int(num) for num in num_you_have.split()}
        )
        for raw_line in lines
        for winning_num, num_you_have in (
            raw_line.split(": ", maxsplit=2)[1].split(" | "),
        )
    )

    part_1 = sum(2 ** (points - 1) for points in card_points if points > 0)

    card_counts = [1 for _ in card_points]
    for i, points in enumerate(card_points):
        for point in range(1, points + 1):
            card_counts[i + point] += card_counts[i]

    part_2 = sum(card_counts)

    return part_1, part_2


print(main())
