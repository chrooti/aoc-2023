import collections

CardScores = tuple[list[int], tuple[int, ...]]

NORMAL_SCORES = {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "T": 10,
    "J": 11,
    "Q": 12,
    "K": 13,
    "A": 14,
}

JOKER_SCORES = {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "T": 10,
    "J": 1,
    "Q": 12,
    "K": 13,
    "A": 14,
}


def compute_score_normal(cards: str) -> CardScores:
    card_scores = tuple(NORMAL_SCORES[card] for card in cards)

    return (
        sorted(collections.Counter(card_scores).values(), reverse=True),
        card_scores,
    )


def compute_score_joker(cards: str) -> CardScores:
    card_scores = tuple(JOKER_SCORES[card] for card in cards)

    groups = collections.Counter(card_scores)
    joker_count = groups.pop(1, 0)

    if len(groups) == 0:
        scores_agg = [5]
    else:
        scores_agg = sorted(groups.values(), reverse=True)
        scores_agg[0] += joker_count

    return scores_agg, card_scores


def scores_to_winnings(hand_scores: tuple[tuple[CardScores, int], ...]):
    return sum(
        i * bid
        for i, (cards, bid) in enumerate(
            sorted(hand_scores, key=lambda hand: hand[0]),
            start=1,
        )
    )


def main():
    with open("inputs/day07.txt", "r") as file:
        lines = file.read().splitlines()

    hands = tuple(
        (cards, int(bid)) for line in lines for (cards, bid) in (line.split(),)
    )

    part_1 = scores_to_winnings(
        tuple((compute_score_normal(cards), bid) for cards, bid in hands)
    )

    part_2 = scores_to_winnings(
        tuple((compute_score_joker(cards), bid) for cards, bid in hands)
    )

    return part_1, part_2


print(main())
