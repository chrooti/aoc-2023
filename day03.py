import re


def main():
    with open("inputs/day03.txt", "r") as file:
        lines = file.read().splitlines()

    max_x = len(lines[0])
    max_y = len(lines)
    cell_pattern = re.compile(r"[0-9]+|[^\.]")

    numbers = []
    symbols = set()
    gears = {}
    for row, line in enumerate(lines):
        for match in cell_pattern.finditer(line):
            value = match[0]

            if value.isdigit():
                numbers.append((int(value), row, match.start(), match.end()))
            elif value == "*":
                gears[(row, match.start())] = []
                symbols.add((row, match.start()))
            else:
                symbols.add((row, match.start()))

    part_1 = 0
    for number, y, x_start, x_end in numbers:
        borders = tuple(
            (cell_y, cell_x)
            for cell_y in range(max(0, y - 1), min(y + 2, max_y))
            for cell_x in range(max(0, x_start - 1), min(x_end + 1, max_x))
            if cell_y != y or cell_x < x_start or cell_x >= x_end
        )

        if any(border in symbols for border in borders):
            part_1 += number

        for border in borders:
            if (gear := gears.get(border)) is not None:
                gear.append(number)

    part_2 = sum(gear[0] * gear[1] for gear in gears.values() if len(gear) == 2)

    return part_1, part_2


print(main())
