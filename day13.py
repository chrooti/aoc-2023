def solve(grid: tuple[tuple[str, ...], ...], smudge: int):
    return next(
        (
            i
            for i in range(1, len(grid))
            if sum(
                a != b
                for line, reflection in zip(reversed(grid[:i]), grid[i:])
                for a, b in zip(line, reflection)
            )
            == smudge
        ),
        0,
    )


def main():
    with open("inputs/day13.txt", "r") as file:
        grids = tuple(
            tuple(tuple(char for char in line) for line in mirror.splitlines())
            for mirror in file.read().split("\n\n")
        )

    part_1 = sum(100 * solve(grid, 0) + solve(tuple(zip(*grid)), 0) for grid in grids)

    part_2 = sum(100 * solve(grid, 1) + solve(tuple(zip(*grid)), 1) for grid in grids)

    return part_1, part_2


print(main())
