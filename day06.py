import math


def clean_input_line(line: str, prefix: str):
    return line.removeprefix(prefix).strip().split()


def solve(time, distance):
    # given that distance is x(time - x)
    # we want to find where the parabola intersects 0
    # AKA - x^2 + time*x = d
    # AKA x^2 - time*x + d = 0
    # so (-b +/- sqrt(b^2 - 4ac)) / 2
    # so (time -/+ sqrt(time^2 - 4*distance) / 2)

    delta = math.sqrt(time**2 - 4 * distance)

    # NOTE: boundaries not included!

    # we want the lowest integer boundary > 0
    # if low == int(low) -> next
    # if low != int(low) -> still next
    low = int((time - delta) / 2) + 1

    # we want the highest integer boundary > 0
    # which means the one before the first integer boundary <= 0
    # if high == int(high) -> previous (current integer is exactly zero)
    # if high != int(high) -> current
    high_exact = (time + delta) / 2
    high_floor = int(high_exact)
    high = high_floor - 1 if high_exact == high_floor else high_floor

    return high - low + 1


def main():
    with open("inputs/day06.txt", "r") as file:
        [raw_times, raw_distances] = file.read().splitlines()

    raw_times = clean_input_line(raw_times, "Time:")
    raw_distances = clean_input_line(raw_distances, "Distance:")

    times = tuple(int(time) for time in raw_times)
    distances = tuple(int(distance) for distance in raw_distances)

    part_1 = math.prod(
        (solve(time, distance) for time, distance in zip(times, distances))
    )

    time = int("".join(raw_times))
    distance = int("".join(raw_distances))

    part_2 = solve(time, distance)

    return part_1, part_2


print(main())
