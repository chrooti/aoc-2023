def tilt(grid: tuple[str, ...]):
    return tuple(
        "#".join(seq.replace("O", "") + "O" * seq.count("O") for seq in line.split("#"))
        for line in grid
    )


def rotate(grid: tuple[str, ...]):
    return tuple("".join(line) for line in zip(*reversed(grid)))


def count(grid: tuple[str, ...]):
    return sum(i + 1 for line in grid for i, cell in enumerate(line) if cell == "O")


def main():
    with open("inputs/day14.txt", "r") as file:
        grid = tuple(file.read().splitlines())

    grid = rotate(grid)
    part_1 = count(tilt(grid))

    seen: dict[tuple[str, ...], int] = {}
    seen_ordered: list[tuple[str, ...]] = []

    steps = 1000000000
    for cycle in range(steps):
        for _ in range(4):
            grid = tilt(grid)
            grid = rotate(grid)

        if (start := seen.get(grid)) is not None:
            cycle_length = cycle - start
            remaining = (steps - cycle) % cycle_length
            grid = seen_ordered[start + remaining - 1]
            break

        seen[grid] = cycle
        seen_ordered.append(grid)

    part_2 = count(grid)

    return part_1, part_2


print(main())
