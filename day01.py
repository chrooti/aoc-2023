def solve(lines: list[str], numbers: tuple[str, ...]):
    return sum(
        10
        * next(
            i % 10
            for offset in range(len(line))
            if line.startswith(numbers, offset)
            for i, number in enumerate(numbers)
            if line.startswith(number, offset)
        )
        + next(
            i % 10
            for offset in range(len(line), 0, -1)
            if line.endswith(numbers, 0, offset)
            for i, number in enumerate(numbers)
            if line.endswith(number, 0, offset)
        )
        for line in lines
    )


def main():
    with open("inputs/day01.txt", "r") as file:
        lines = file.read().splitlines()

    return (
        # fmt: off
        solve(lines, ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")),
        solve(lines, ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine")),
        # fmt: on
    )


print(main())
