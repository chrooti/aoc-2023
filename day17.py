import heapq


def solve(grid: tuple[tuple[int, ...]], min_steps: int, max_steps: int):
    max_x = len(grid[0])
    max_y = len(grid)

    heat_losses = [
        [[int(2**32), int(2**32)] for _ in range(max_x)] for _ in range(max_y)
    ]
    seen: set[tuple[int, int]] = set()

    q = [(0, 0, 0, 0), (0, 0, 0, 1)]
    while q:
        heat_loss, y, x, is_horizontal = heapq.heappop(q)

        if (y, x, is_horizontal) in seen:
            continue
        seen.add((y, x, is_horizontal))

        if y == max_y - 1 and x == max_x - 1:
            return heat_loss

        turns = ((1, 0), (-1, 0)) if is_horizontal == 0 else ((0, 1), (0, -1))
        for y_off, x_off in turns:
            new_heat_loss = heat_loss
            new_is_horizontal = int(x_off == 0)

            for i in range(1, max_steps):
                new_y = y + i * y_off
                new_x = x + i * x_off
                if not (0 <= new_y < max_x and 0 <= new_x < max_y):
                    break

                new_heat_loss += grid[new_y][new_x]

                if i < min_steps:
                    continue

                if new_heat_loss >= heat_losses[new_y][new_x][new_is_horizontal]:
                    continue

                heapq.heappush(q, (new_heat_loss, new_y, new_x, new_is_horizontal))
                heat_losses[new_y][new_x][new_is_horizontal] = new_heat_loss


def main():
    with open("inputs/day17.txt", "r") as file:
        grid = tuple(
            tuple(int(heat) for heat in line) for line in file.read().splitlines()
        )

    part_1 = solve(grid, 1, 4)
    part_2 = solve(grid, 4, 11)

    return part_1, part_2


print(main())
