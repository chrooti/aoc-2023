Coords = tuple[int, int]


def solve(
    grid: list[str],
    point: Coords,
    direction: Coords,
):
    max_x = len(grid[0])
    max_y = len(grid)

    seen: set[tuple[Coords, Coords]] = set()
    energized: set[Coords] = set()

    stack: list[tuple[Coords, Coords]] = [(point, direction)]
    while stack:
        (y, x), (y_off, x_off) = stack.pop()

        while True:
            y += y_off
            x += x_off

            if not (0 <= y < max_y and 0 <= x < max_x):
                break

            point = (y, x)
            direction = (y_off, x_off)
            if (point, direction) in seen:
                break

            seen.add((point, direction))
            energized.add(point)
            cell = grid[y][x]

            if cell == "|" and y_off == 0:
                stack.append(((y, x), (1, 0)))
                y_off, x_off = -1, 0
            elif cell == "-" and x_off == 0:
                stack.append(((y, x), (0, 1)))
                y_off, x_off = 0, -1
            elif cell == "\\":
                x_off, y_off = y_off, x_off
            elif cell == "/":
                x_off, y_off = -y_off, -x_off

    return len(energized)


def main():
    with open("inputs/day16.txt", "r") as file:
        grid = file.read().splitlines()

    part_1 = solve(grid, (0, -1), (0, 1))

    max_x = len(grid[0])
    max_y = len(grid)

    part_2 = max(
        *(
            max(
                solve(grid, (-1, x), (1, 0)),
                solve(grid, (max_y, x), (-1, 0)),
            )
            for x in range(max_x)
        ),
        *(
            max(
                solve(grid, (y, -1), (0, 1)),
                solve(grid, (y, max_x), (0, -1)),
            )
            for y in range(max_y)
        )
    )

    return part_1, part_2


print(main())
