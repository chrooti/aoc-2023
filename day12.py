def solve(lava: str, groups: tuple[int, ...]):
    # I solved this via functools.cache and recursion, but this method
    # from https://github.com/mfornet/advent-of-code-2023/blob/main/src/bin/12.rs
    # feels much more in the spirit of actual dynamic programming.

    # dots at the end or the beginning don't add any way of arranging the springs
    lava.strip(".")

    # since the current position in the counts array sits at i+1
    # adding a "." at the start makes it so that i - group can never be negative
    # and also makes the "is not #" check work
    lava = f".{lava}"

    # how many ways exist to satisfy the current "group"
    # (and the previous ones) when the "lava" index is i-1?
    counts = [0] * (len(lava) + 1)

    counts[0] = 1
    for i, x in enumerate(lava):
        if x == "#":
            break
        counts[i + 1] = 1

    for group in groups:
        next_counts = [0] * (len(lava) + 1)
        chunk = 0

        for i, cell in enumerate(lava):
            # for each cell we can do two things:
            # 1. not add a # --> propagate the count from before
            # 2. add a # -->
            #   - check if the current chunk of #|? is long enough
            #   - check if i - group doesn't contain a "#"
            #   - if both are valid we add the number of ways we can fit
            #     the previous springs (counts[i - group])

            if cell == ".":
                # only 1.
                chunk = 0
                next_counts[i + 1] += next_counts[i]
            elif cell == "#":
                # only 2.
                chunk += 1
                if chunk >= group and lava[i - group] != "#":
                    next_counts[i + 1] += counts[i - group]
            else:
                # both 1. and 2.
                chunk += 1
                next_counts[i + 1] += next_counts[i]
                if chunk >= group and lava[i - group] != "#":
                    next_counts[i + 1] += counts[i - group]

        counts = next_counts

    return counts[-1]


def main():
    with open("inputs/day12.txt", "r") as file:
        lines = file.read().splitlines()

    conditions = tuple(
        (lava, tuple(int(group) for group in groups.split(",")))
        for line in lines
        for (lava, groups) in (line.split(),)
    )

    part_1 = sum(solve(lava, groups) for lava, groups in conditions)
    part_2 = sum(solve("?".join([lava] * 5), groups * 5) for lava, groups in conditions)
    return part_1, part_2


print(main())
