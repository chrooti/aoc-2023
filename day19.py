import math


def main():
    with open("inputs/day19.txt", "r") as file:
        [raw_workflows, raw_parts] = file.read().split("\n\n")

    workflows = {}
    for raw_workflow in raw_workflows.splitlines():
        name, raw_instructions = raw_workflow.split("{")
        [*raw_conditions, fallback] = raw_instructions.split(",")
        conditions = []
        for condition in raw_conditions:
            instruction, outcome = condition.split(":")
            part = instruction[0]
            op = instruction[1]
            val = int(instruction[2:])
            if op == "<":
                conditions.append((part, (0, val, val, 4001), outcome))
            else:
                conditions.append((part, (val + 1, 4001, 0, val + 1), outcome))

        workflows[name] = (conditions, fallback[:-1])

    parts = []
    for raw_part in raw_parts.splitlines():
        ps = raw_part[1:-1].split(",")
        parts.append({p[0]: int(p[2:]) for p in ps})

    accepted_parts = []
    for part in parts:
        workflow_name = "in"
        while True:
            workflow = workflows[workflow_name]
            conditions, fallback = workflow
            for checked_part, (low, high, _, _), outcome in conditions:
                if low <= part[checked_part] < high:
                    workflow_name = outcome
                    break
            else:
                workflow_name = fallback

            if workflow_name == "A":
                accepted_parts.append(part)
                break
            elif workflow_name == "R":
                break

    part_1 = sum(sum(x.values()) for x in accepted_parts)

    q = [("in", {"x": (1, 4001), "m": (1, 4001), "a": (1, 4001), "s": (1, 4001)})]
    part_2 = 0
    while q:
        workflow_name, parts = q.pop()

        if workflow_name == "A":
            part_2 += math.prod(xx[1] - xx[0] for xx in parts.values())
            continue
        elif workflow_name == "R":
            continue

        workflow = workflows[workflow_name]
        conditions, fallback = workflow

        new_parts = parts.copy()
        for checked_part, (low, high, inv_low, inv_high), outcome in conditions:
            old_lo, old_hi = new_parts[checked_part]
            new_part = (max(old_lo, low), min(old_hi, high))
            inv_new_part = (max(old_lo, inv_low), min(old_hi, inv_high))

            if new_part[0] >= new_part[1]:
                break

            new_parts[checked_part] = new_part
            q.append((outcome, new_parts.copy()))
            new_parts[checked_part] = inv_new_part
        else:
            q.append((fallback, new_parts))

    return part_1, part_2


print(main())
