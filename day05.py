# dest, dest_end, src, src_end
Map = tuple[int, int, int, int]


def main():
    with open("inputs/day05.txt", "r") as file:
        [raw_seeds, _blank, *raw_maps] = file.read().splitlines()

    seeds = tuple(int(seed) for seed in raw_seeds.removeprefix("seeds: ").split())

    seed_iter = iter(seeds)
    starting_seed_ranges = tuple(
        (start, start + length) for start, length in zip(seed_iter, seed_iter)
    )

    maps: list[list[Map]] = []
    current_map: list[Map] = []
    for raw_map in raw_maps:
        if not raw_map:
            maps.append(current_map)
            current_map = None  # type: ignore
        elif not raw_map[0].isdigit():
            current_map = []
        else:
            dest, src, count = raw_map.split()
            dest = int(dest)
            src = int(src)
            count = int(count)
            current_map.append((dest, dest + count, src, src + count))

    maps.append(current_map)
    current_map = None  # type: ignore

    for map_ in maps:
        seeds = tuple(
            next(
                (
                    dest_start + seed - src_start
                    for dest_start, dest_end, src_start, src_end in map_
                    if src_start <= seed < src_end
                ),
                seed,
            )
            for seed in seeds
        )
    part_1 = min(seeds)

    part_2 = int(2**64 - 1)
    for starting_seed_range in starting_seed_ranges:
        seed_ranges = [starting_seed_range]
        for map_ in maps:
            next_ranges = []
            while seed_ranges:
                seed_start, seed_end = seed_ranges.pop()
                for dest_start, dest_end, src_start, src_end in map_:
                    # ( -> seed, [ -> src
                    if seed_end <= src_start or seed_start >= src_end:
                        # ) [ or ] (
                        continue
                    elif src_start <= seed_start < seed_end <= src_end:
                        # [ ( ) ]
                        # fully covered: just remap
                        offset = dest_start - src_start
                        next_ranges.append((seed_start + offset, seed_end + offset))
                        break
                    elif src_start <= seed_start < src_end <= seed_end:
                        # [ ( ] )
                        # left overlap -> remap left, reprocess right
                        offset = dest_start - src_start
                        next_ranges.append((seed_start + offset, dest_end))
                        seed_ranges.append((src_end, seed_end))
                        break
                    elif seed_start <= src_start < seed_end <= src_end:
                        # ( [ ) ]
                        # right overlap -> reprocess left, remap right
                        offset = dest_start - src_start
                        seed_ranges.append((seed_start, src_start))
                        next_ranges.append((dest_start, seed_end + offset))
                        break
                    elif seed_start < src_start <= src_end < seed_end:
                        # ( [ ] )
                        # full range overlap -> reprocess left, remap center, reprocess right
                        next_ranges.append((dest_start, dest_end))
                        seed_ranges.extend(
                            ((seed_start, src_start), (src_end, seed_end))
                        )
                        break
                    else:
                        raise Exception(
                            f"{seed_start=} {seed_end=} {src_start=} {src_end=}"
                        )
                else:
                    next_ranges.append((seed_start, seed_end))

            seed_ranges = next_ranges

        part_2 = min(part_2, min(seed_start for seed_start, seed_end in seed_ranges))

    return part_1, part_2


print(main())
