import re


def main():
    with open("inputs/day02.txt", "r") as file:
        lines = file.read().splitlines()

    cube_pattern = re.compile(r"(\d+) (red|green|blue)")

    part_1 = sum(
        int(raw_game_id.removeprefix("Game "))
        for line in lines
        for raw_game_id, raw_rounds in (line.split(": ", maxsplit=2),)
        if all(
            (color == "red" and count <= 12)
            or (color == "green" and count <= 13)
            or (color == "blue" and count <= 14)
            for cube in cube_pattern.finditer(raw_rounds)
            for count, color in ((int(cube[1]), cube[2]),)
        )
    )

    part_2 = sum(
        max(count for count, color in cubes if color == "red")
        * max(count for count, color in cubes if color == "green")
        * max(count for count, color in cubes if color == "blue")
        for line in lines
        for cubes in (
            tuple((int(cube[1]), cube[2]) for cube in cube_pattern.finditer(line)),
        )
    )

    return part_1, part_2


print(main())
