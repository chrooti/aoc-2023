import math
import re


def main():
    with open("inputs/day08.txt", "r") as file:
        [directions, _, *raw_nodes] = file.read().splitlines()

    directions = tuple(0 if direction == "L" else 1 for direction in directions)
    direction_count = len(directions)

    node_regex = re.compile(r"([A-Z]{3}) = \(([A-Z]{3}), ([A-Z]{3})\)")
    nodes = {
        node: (left, right)
        for raw_node in raw_nodes
        for node, left, right in (node_regex.match(raw_node).groups(),)  # type: ignore
    }

    node = "AAA"
    part_1 = 0
    while node != "ZZZ":
        direction = directions[part_1 % direction_count]
        node = nodes[node][direction]
        part_1 += 1

    # generic solution would actually be:
    # - finding A = length to reach cycle, B = cycle length
    # - using CRT where x = Ai % (Ai + Bi)
    starts = tuple(node for node in nodes if node.endswith("A"))
    part_2 = 1
    for node in starts:
        count = 0
        while not node.endswith("Z"):
            direction = directions[count % direction_count]
            node = nodes[node][direction]
            count += 1

        part_2 = math.lcm(part_2, count)

    return part_1, part_2


print(main())
