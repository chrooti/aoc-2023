def main():
    with open("inputs/day10.txt", "r") as file:
        grid = file.read().splitlines()

    for y, line in enumerate(grid):
        if (start_x := line.find("S")) != -1:
            start_y = y
            break
    else:
        raise Exception("Start not found")

    # _entrance_ directions
    directions = {
        "|": ((0, 1), (0, -1)),
        "-": ((1, 0), (-1, 0)),
        "L": ((0, 1), (-1, 0)),
        "J": ((0, 1), (1, 0)),
        "7": ((0, -1), (1, 0)),
        "F": ((0, -1), (-1, 0)),
    }

    location = x, y = next(
        (x, y)
        for x_offset, y_offset in ((0, -1), (0, 1), (1, 0), (-1, 0))
        for x, y in ((start_x + x_offset, start_y + y_offset),)
        if (-x_offset, -y_offset) in directions[grid[y][x]]
    )
    direction = (x - start_x, y - start_y)

    perimeter = 1
    area = 0
    while location != (start_x, start_y):
        x, y = location
        x_offset, y_offset = next(
            offset for offset in directions[grid[y][x]] if offset != direction
        )

        # enter with one direction, exit with the other inverted
        x_offset = -x_offset
        y_offset = -y_offset

        direction = (x_offset, y_offset)
        location = (x + x_offset, y + y_offset)

        perimeter += 1
        area += y * x_offset

    part_1 = perimeter // 2

    # originally solved by implementing ray casting
    # https://en.m.wikipedia.org/wiki/Point_in_polygon
    #
    # FOR THE SECOND ADVENT IN A ROW I MISSED THE FOLLOWING:
    # - consider paths between cell centers (0, 0) becomes (0.5, 0.5)
    # - | and - cells count only half area
    # - other bends contribute either 1/4 (outer) or 3/4 (inner), so halving is still accurate, but...
    # - there are 4 outers more than inner (+1)
    part_2 = area - perimeter // 2 + 1

    return part_1, part_2


print(main())
