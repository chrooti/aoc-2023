def compute_hash(value: str):
    hash_value = 0
    for c in value:
        hash_value = (hash_value + ord(c)) * 17 % 256

    return hash_value


def lookup(box: list[tuple[str, int]], label: str):
    return next((i for i, (label_, _) in enumerate(box) if label == label_), -1)


def main():
    with open("inputs/day15.txt", "r") as file:
        sequences = file.read().strip().split(",")

    part_1 = sum(compute_hash(seq) for seq in sequences)

    boxes: list[list[tuple[str, int]]] = [[] for _ in range(256)]
    for seq in sequences:
        if seq.endswith("-"):
            label = seq[:-1]
            box = boxes[compute_hash(label)]

            i = lookup(box, label)
            if i != -1:
                box.pop(i)
        else:
            label = seq[:-2]
            box = boxes[compute_hash(label)]
            focal_length = int(seq[-1])

            i = lookup(box, label)
            if i == -1:
                box.append((label, focal_length))
            else:
                box[i] = (label, focal_length)

    part_2 = sum(
        i * slot * focal_length
        for i, box in enumerate(boxes, start=1)
        for slot, (_, focal_length) in enumerate(box, start=1)
    )

    return part_1, part_2


print(main())
