import itertools


def main():
    with open("inputs/day09.txt", "r") as file:
        lines = file.read().splitlines()

    sequences = tuple(tuple(int(value) for value in line.split()) for line in lines)

    part_1 = part_2 = 0
    for sequence in sequences:
        subsequences = [sequence]
        while any(x != sequence[0] for x in sequence):
            sequence = tuple(b - a for a, b in itertools.pairwise(sequence))
            subsequences.append(sequence)

        next_value = prev_value = 0
        for [first, *_, last] in reversed(subsequences):
            next_value = last + next_value
            prev_value = first - prev_value

        part_1 += next_value
        part_2 += prev_value

    return part_1, part_2


print(main())
