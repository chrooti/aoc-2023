import itertools


def sum_distances(grid: list[str], delta: int):
    columns = len(grid[0])

    x_offset = 0
    column_indices = []
    for i in range(columns):
        column_indices.append(i + x_offset)
        if all(line[i] == "." for line in grid):
            x_offset += delta

    galaxies = []
    y_offset = 0
    for y, line in enumerate(grid):
        x = line.find("#")
        if x == -1:
            y_offset += delta
            continue

        galaxies.append((y + y_offset, column_indices[x]))
        x = line.find("#", x + 1)

        while x != -1:
            galaxies.append((y + y_offset, column_indices[x]))
            x = line.find("#", x + 1)

    return sum(
        abs(y1 - y2) + abs(x1 - x2)
        for i, (y1, x1) in enumerate(galaxies)
        for y2, x2 in itertools.islice(galaxies, i + 1, None)
    )


def main():
    with open("inputs/day11.txt", "r") as file:
        grid = file.read().splitlines()

    part_1 = sum_distances(grid, 1)
    part_2 = sum_distances(grid, 1000000 - 1)

    return part_1, part_2


print(main())
